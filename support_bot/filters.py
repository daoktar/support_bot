from telegram import Message
from telegram.ext import BaseFilter


class _ReplyToBot(BaseFilter):
    def filter(self, message: Message) -> bool:
        return (
            message.reply_to_message
            and message.reply_to_message.from_user.id == message.bot.id
        )


reply_to_bot = _ReplyToBot()
