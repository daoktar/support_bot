FROM inn0kenty/pyinstaller-alpine:3.7 as builder

ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=on \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100

WORKDIR /build

COPY poetry.lock .
COPY pyproject.toml .

RUN poetry config settings.virtualenvs.create false \
    && poetry install --no-dev --no-interaction

COPY . .

RUN /pyinstaller/pyinstaller.sh \
    --skip-req-install \
    --random-key \
    --noconfirm \
    --clean \
    --onefile \
    --name app \
    support_bot/main.py

FROM alpine:3.9

RUN addgroup -g 1000 -S app \
    && adduser -S -u 1000 -G app app \
    && mkdir /storage \
    && chown -R app:app /storage

LABEL maintainer="Innokenty Lebedev <i.lebedev@letnyayashkola.org>"

COPY --from=builder --chown=1000:1000 /build/dist/app .

USER app

ENTRYPOINT ["./app"]
